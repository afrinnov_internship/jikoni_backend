package com.afrinnov.jikoni.ingredient;

import com.afrinnov.jikoni.recipe.Recipe;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IngredientDto {
    private String ingredientName;
    private Double ingredientQuantity;
    private String ingredientUnite;
}
