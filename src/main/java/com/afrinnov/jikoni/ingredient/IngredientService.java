package com.afrinnov.jikoni.ingredient;

import com.afrinnov.jikoni.commons.*;
import org.springframework.stereotype.Service;

@Service
public class IngredientService extends AbstractService<Ingredient, IngredientDto, IngredientForm> {
    private final IngredientRepository ingredientRepository;
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    protected boolean checkAlreadyEntity() {
        return false;
    }

    @Override
    protected IRepository<Ingredient> getRepository() {
        return this.ingredientRepository;
    }

    @Override
    protected AbstractMapper<Ingredient, IngredientDto, IngredientForm> getMapper() {
        return new IngredientMapper();
    }

    @Override
    public IngredientDto save(IngredientForm ingredientForm) {
        Ingredient ingredient = this.getMapper().fromFormToEntity(ingredientForm);
        ingredient.setDeleted(false);
        ingredient = this.ingredientRepository.save(ingredient);
        return this.getMapper().fromEntityToDto(ingredient);
    }

    @Override
    public IngredientDto detailsRecipe(String code) {
        return null;
    }

    public Ingredient save(String name){
        Ingredient ingredient = new Ingredient();
        ingredient.setName(name);
        ingredient.setCode(Util.generateCode(PrefixCode.INGREDIENT));
        return ingredientRepository.save(ingredient);
    }

    public Ingredient getIngredientFromName(String name){
        return ingredientRepository.findByName(name);
    }

}
