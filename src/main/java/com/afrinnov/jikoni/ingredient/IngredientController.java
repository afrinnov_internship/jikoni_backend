package com.afrinnov.jikoni.ingredient;

import com.afrinnov.jikoni.recipe.RecipeDto;
import com.afrinnov.jikoni.recipe.RecipeForm;
import com.afrinnov.jikoni.recipe.RecipeService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/ingredient")
@CrossOrigin("*")
public class IngredientController {
    IngredientService ingredientService;

    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @PostMapping("/create")
    public IngredientDto createRecipe (@RequestBody IngredientForm ingredientForm) {
        return this.ingredientService.save(ingredientForm);
    }
}
