package com.afrinnov.jikoni.ingredient;

import com.afrinnov.jikoni.commons.IRepository;
import com.afrinnov.jikoni.recipe.Recipe;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRepository extends IRepository<Ingredient> {
    List<Ingredient> findAllByCode(String code);

    Ingredient findByName(String name);
}
