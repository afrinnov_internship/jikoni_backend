package com.afrinnov.jikoni.ingredient;

import com.afrinnov.jikoni.commons.AbstractMapper;
import com.afrinnov.jikoni.commons.PrefixCode;
import com.afrinnov.jikoni.commons.Util;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class IngredientMapper extends AbstractMapper<Ingredient, IngredientDto, IngredientForm> {
    @Override
    public Ingredient fromDtoToEntity(IngredientDto ingredientDto) {
        Ingredient ingredient = new Ingredient();
        if (ingredientDto != null) {
            ingredient.setDeleted(false);
            ingredient.setName(ingredientDto.getIngredientName().toLowerCase().trim());
        }
        return ingredient;
    }

    @Override
    public Ingredient fromFormToEntity(IngredientForm ingredientForm) {
        Ingredient ingredient = new Ingredient();
        if (ingredientForm != null) {
            ingredient.setCode(Util.generateCode(PrefixCode.INGREDIENT));
            ingredient.setName(ingredientForm.getIngredientName().toLowerCase().trim());

        }
        return ingredient;
    }

    @Override
    public IngredientDto fromEntityToDto(Ingredient ingredient) {
        IngredientDto ingredientDto = new IngredientDto();

        if (ingredient != null) {
            ingredientDto.setIngredientName(ingredient.getName());
        }

        return ingredientDto;
    }
}
