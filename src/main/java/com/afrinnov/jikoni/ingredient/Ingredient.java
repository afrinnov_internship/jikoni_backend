package com.afrinnov.jikoni.ingredient;


import com.afrinnov.jikoni.commons.AbstractEntity;
import com.afrinnov.jikoni.recipe.Recipe;
import com.afrinnov.jikoni.recipeIngredient.RecipeIngredient;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(
        name = "ingredients",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "name"),
        }
)
public class Ingredient  extends AbstractEntity {

    @Column(name = "name", nullable = false, updatable = false)
    private String name;

    @OneToMany(mappedBy = "ingredient")
    private List<RecipeIngredient> recipeIngredients;

}
