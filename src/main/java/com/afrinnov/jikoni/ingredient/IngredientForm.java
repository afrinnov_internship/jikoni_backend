package com.afrinnov.jikoni.ingredient;

import com.afrinnov.jikoni.recipe.Recipe;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IngredientForm { // name - description - quantity - recipe

    private String ingredientName;
    private Double ingredientQuantity;
    private String ingredientUnit;

}
