package com.afrinnov.jikoni.recipeIngredient;

import com.afrinnov.jikoni.commons.AbstractEntity;
import com.afrinnov.jikoni.commons.Util;
import com.afrinnov.jikoni.ingredient.Ingredient;
import com.afrinnov.jikoni.recipe.Recipe;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "recipe_ingredient")
public class RecipeIngredient implements Serializable {

    private static final long serialVersionUID = -1089761391695703597L;

    @EmbeddedId
    private RecipeIngredientId idRecipeIngredients = new RecipeIngredientId();

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    @MapsId("recipeId")
    Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "ingredient_id")
    @MapsId("ingredientId")
    Ingredient ingredient;

    @Column(name = "quantity")
    private double quantity;

    @Column(name = "unit")
    private String unit;

    @Column(name = "code", nullable = false, updatable = false)
    protected String code;

    @CreatedDate
    @Column(name = "created_date", updatable = false)
    protected Instant createdDate = Instant.now();

    @LastModifiedDate
    @Column(name = "last_modified_date")
    protected Instant lastModifiedDate = Instant.now();

    @CreatedBy
    @Column(name = "created_by")
    protected String createdBy = Util.DEFAULT_USER;

    @LastModifiedBy
    @Column(name = "last_modified_by")
    protected String lastModifiedBy = Util.DEFAULT_USER;

    @Column(name = "is_deleted")
    protected boolean isDeleted = false;
}
