package com.afrinnov.jikoni.recipeIngredient;

import com.afrinnov.jikoni.commons.IRepository;
import com.afrinnov.jikoni.commons.PrefixCode;
import com.afrinnov.jikoni.commons.Util;
import com.afrinnov.jikoni.ingredient.Ingredient;
import com.afrinnov.jikoni.recipe.Recipe;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class RecipeIngredientService {

    private RecipeIngredientRepository recipeIngredientRepository;

    protected IRepository<RecipeIngredient> getRepository(){
        return this.recipeIngredientRepository;
    }

    public RecipeIngredient save(Recipe recipe, Ingredient ingredient, double quantity, String unit){
        RecipeIngredient recipeIngredient = new RecipeIngredient();
        recipeIngredient.setRecipe(recipe);
        recipeIngredient.setIngredient(ingredient);
        recipeIngredient.setQuantity(quantity);
        recipeIngredient.setUnit(unit);
        recipeIngredient.setCode(Util.generateCode(PrefixCode.RECIPE_INGREDIENT));
        return recipeIngredientRepository.save(recipeIngredient);

    }

}
