package com.afrinnov.jikoni.recipeIngredient;

import com.afrinnov.jikoni.commons.IRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeIngredientRepository extends IRepository<RecipeIngredient> {

}
