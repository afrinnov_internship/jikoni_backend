package com.afrinnov.jikoni.commons;

import java.util.Optional;

public abstract class AbstractService<ENTITY, DTO, FORM> {
    //private static final IRepository = ;
    protected abstract boolean checkAlreadyEntity();
    protected abstract IRepository<ENTITY> getRepository();
    protected abstract AbstractMapper<ENTITY, DTO, FORM> getMapper();

    public DTO save(FORM form) {
        ENTITY entity = this.getMapper().fromFormToEntity(form);
        entity = getRepository().save(entity);
        DTO dto = this.getMapper().fromEntityToDto(entity);
        return dto;
    }

    public DTO getByCode(String code) {
        Optional<ENTITY> entity = this.getRepository().findByCode(code);

        if (entity.isPresent()) {
            return this.getMapper().fromEntityToDto(entity.get());
        }

        return null;
    }

    public abstract DTO detailsRecipe(String code);

//    public abstract Page<Recipe> findAllRecipe(int page, int length);
}
