package com.afrinnov.jikoni.commons;

import org.springframework.data.domain.Pageable;

public abstract class AbstractMapper<ENTITY, DTO, FORM> {
    public abstract ENTITY fromDtoToEntity(DTO dto);
    public abstract ENTITY fromFormToEntity(FORM form);
    public abstract DTO fromEntityToDto(ENTITY entity);

}
