package com.afrinnov.jikoni.commons;

public enum PrefixCode {
    RECIPE,
    INGREDIENT,
    RECIPE_INGREDIENT,
    ;

    @Override
    public String toString() {
        switch (this) {
            case RECIPE:
                return "RCP";
            case INGREDIENT:
                return "ING";
            case RECIPE_INGREDIENT:
                return "RECP-ING";
            default:
                return null;
        }
    }


}

