package com.afrinnov.jikoni.commons;

import java.util.UUID;

public class Util {
    public static String DEFAULT_USER = "SYSTEM";

    public static String generateCode(PrefixCode prefix) {
        return prefix.toString() + UUID.randomUUID().toString();
    }
}
