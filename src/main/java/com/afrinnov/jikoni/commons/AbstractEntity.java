package com.afrinnov.jikoni.commons;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.time.Instant;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AbstractEntity {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "code", nullable = false, updatable = false)
    protected String code;

    @CreatedDate
    @Column(name = "created_date", updatable = false)
    protected Instant createdDate = Instant.now();

    @LastModifiedDate
    @Column(name = "last_modified_date")
    protected Instant lastModifiedDate = Instant.now();

    @CreatedBy
    @Column(name = "created_by")
    protected String createdBy = Util.DEFAULT_USER;

    @LastModifiedBy
    @Column(name = "last_modified_by")
    protected String lastModifiedBy = Util.DEFAULT_USER;

    @Column(name = "is_deleted")
    protected boolean isDeleted = false;

}
