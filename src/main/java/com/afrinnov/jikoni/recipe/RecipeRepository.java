package com.afrinnov.jikoni.recipe;

import com.afrinnov.jikoni.commons.IRepository;
import com.afrinnov.jikoni.ingredient.Ingredient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface RecipeRepository extends IRepository<Recipe> {


    List<Recipe> findAllByCreatedDate(Instant creationDate, Pageable pageable);
}
