package com.afrinnov.jikoni.recipe;

import com.afrinnov.jikoni.ingredient.Ingredient;
import com.afrinnov.jikoni.ingredient.IngredientForm;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecipeForm  {
    private String recipeLabel;
    private String recipeDescription;
    private List<IngredientForm> recipeIngredients;
    private String recipeImageUrl;

}
