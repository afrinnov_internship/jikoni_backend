package com.afrinnov.jikoni.recipe;

import com.afrinnov.jikoni.ingredient.IngredientDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecipeDto {
    private String recipeCode;
    private String recipeLabel;
    private String recipeDescription;
    private String recipeAuthor;
    private String recipeImageUrl;
    private List<IngredientDto> recipeIngredients;
    private Instant recipeModificationDate;

}
