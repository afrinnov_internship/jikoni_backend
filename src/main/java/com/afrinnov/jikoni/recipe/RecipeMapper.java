package com.afrinnov.jikoni.recipe;

import com.afrinnov.jikoni.commons.AbstractMapper;
import com.afrinnov.jikoni.commons.PrefixCode;
import com.afrinnov.jikoni.commons.Util;
import com.afrinnov.jikoni.ingredient.IngredientDto;
import com.afrinnov.jikoni.ingredient.IngredientMapper;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class RecipeMapper extends AbstractMapper<Recipe, RecipeDto, RecipeForm> {
    RecipeRepository recipeRepository;
    @Override
    public Recipe fromDtoToEntity(RecipeDto recipeDto) {
        Recipe recipe = new Recipe();

        if (recipeDto != null) {
            IngredientMapper ingredientMapper = new IngredientMapper();

            recipe.setLabel(recipeDto.getRecipeLabel().toLowerCase().trim());
            recipe.setDescription(recipeDto.getRecipeDescription());
            recipe.setCode(recipeDto.getRecipeCode());
            recipe.setImagePath(recipeDto.getRecipeImageUrl());
            recipe.setLastModifiedDate(recipeDto.getRecipeModificationDate());
            recipe.setLastModifiedBy(Util.DEFAULT_USER);
            /*
            recipe.setIngredients(recipeDto
                    .getRecipeIngredients()
                    .stream()
                    .map(ingredientDto -> ingredientMapper.fromDtoToEntity(ingredientDto))
                    .collect(Collectors.toList())
            );

             */
        }

        return recipe;
    }

    @Override
    public Recipe fromFormToEntity(RecipeForm recipeForm) {
        Recipe recipe = new Recipe();

        if (recipeForm != null) {
            IngredientMapper ingredientMapper = new IngredientMapper();

            recipe.setDeleted(false);
            recipe.setCode(Util.generateCode(PrefixCode.RECIPE));
            recipe.setLabel(recipeForm.getRecipeLabel().toLowerCase().trim());
            recipe.setDescription(recipeForm.getRecipeDescription());
            recipe.setImagePath(recipeForm.getRecipeImageUrl());
            recipe.setCreatedBy(Util.DEFAULT_USER);
            /*
            if (recipeForm.getRecipeIngredients() != null) {
                recipe.setIngredients(recipeForm
                        .getRecipeIngredients()
                        .stream()
                        .map(ingredientForm -> ingredientMapper.fromFormToEntity(ingredientForm))
                        .collect(Collectors.toList())
                );
            }

             */
        }
        return recipe;
    }

    @Override
    public RecipeDto fromEntityToDto(Recipe recipe) {
        RecipeDto recipeDto = new RecipeDto();

        if (recipe != null) {
            IngredientMapper ingredientMapper = new IngredientMapper();

            recipeDto.setRecipeLabel(recipe.getLabel());
            recipeDto.setRecipeDescription(recipe.getDescription());
            recipeDto.setRecipeCode(recipe.getCode());
            recipeDto.setRecipeModificationDate(recipe.getLastModifiedDate());
            recipeDto.setRecipeImageUrl(recipe.getImagePath());
            recipeDto.setRecipeAuthor(Util.DEFAULT_USER);
            if(recipe.getRecipeIngredients() != null){
                recipeDto.setRecipeIngredients(recipe
                        .getRecipeIngredients()
                        .stream()
                        .map((recipeIngredient) -> {
                            IngredientDto ingredientDto = new IngredientDto();
                            ingredientDto.setIngredientQuantity(recipeIngredient.getQuantity());
                            ingredientDto.setIngredientUnite(recipeIngredient.getUnit());
                            ingredientDto.setIngredientName(recipeIngredient.getIngredient().getName());
                            return ingredientDto;
                        })
                        .collect(Collectors.toList())
                );
            }
            /*
            if (recipe.getIngredients() != null) {
                recipeDto.setRecipeIngredients(recipe
                        .getIngredients()
                        .stream()
                        .map(ingredient -> ingredientMapper.fromEntityToDto(ingredient))
                        .collect(Collectors.toList())
                );
            }

             */
        }

        return recipeDto;
    }

  /*  public Page<RecipeDto> fromEntityPageToDtoPageable (Page<Recipe> recipePage) {
        List<RecipeDto> recipeDtos = recipePage.stream().map(this::fromEntityToDto).collect(Collectors.toList());
        Page<RecipeDto> dtoPage = new PageImpl<>(recipeDtos, recipePage.getPageable(), recipePage.getTotalElements());


        return dtoPage;
    }*/


}
