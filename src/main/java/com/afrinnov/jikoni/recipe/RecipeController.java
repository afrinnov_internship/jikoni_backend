package com.afrinnov.jikoni.recipe;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/recipe")
public class RecipeController {
    private final RecipeService recipeService;

    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @PostMapping("/create")
    public RecipeDto createRecipe (@RequestBody RecipeForm recipeForm) {
        return this.recipeService.createRecipe(recipeForm);
    }

    @GetMapping("/details/{code}")
    public RecipeDto detailsRecipe (@PathVariable String code) {

        return this.recipeService.getByCode(code);
    }

    @GetMapping("/{page}/{length}")
    public Page<RecipeDto> allRecipe (@PathVariable int page, @PathVariable int length) {

        return recipeService.findAllRecipe(page, length);
    }


}
