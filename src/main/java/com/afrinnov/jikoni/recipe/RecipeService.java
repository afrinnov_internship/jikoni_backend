package com.afrinnov.jikoni.recipe;

import com.afrinnov.jikoni.commons.*;
import com.afrinnov.jikoni.ingredient.*;
import com.afrinnov.jikoni.recipeIngredient.RecipeIngredient;
import com.afrinnov.jikoni.recipeIngredient.RecipeIngredientRepository;
import com.afrinnov.jikoni.recipeIngredient.RecipeIngredientService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class RecipeService extends AbstractService<Recipe, RecipeDto, RecipeForm> {
    
    private final RecipeRepository recipeRepository;
    
    private final RecipeIngredientRepository recipeIngredientRepository;
    private final IngredientService ingredientService;

    private final IngredientRepository ingredientRepository;

    private final RecipeIngredientService recipeIngredientService;

    @Override
    public boolean checkAlreadyEntity() {

        return false;
    }

    @Override
    protected IRepository<Recipe> getRepository() {
        return this.recipeRepository;
    }

    @Override
    protected AbstractMapper<Recipe, RecipeDto, RecipeForm> getMapper() {
        return new RecipeMapper();
    }

    public RecipeDto createRecipe(RecipeForm recipeForm) {
        AbstractMapper<Recipe, RecipeDto, RecipeForm> mapper = getMapper();
        Recipe recipe = mapper.fromFormToEntity(recipeForm);
        recipeRepository.save(recipe);
        for (IngredientForm ingredientForm : recipeForm.getRecipeIngredients() ){
            Ingredient ingredient = getOrCreateIngredient(ingredientForm);
            attachIngredientToRecipe(recipe, ingredientForm, ingredient);
        }
        return mapper.fromEntityToDto(recipe);
    }

    private void attachIngredientToRecipe(Recipe recipe, IngredientForm ingredientForm, Ingredient ingredient) {
        RecipeIngredient recipeIngredient = new RecipeIngredient();
        recipeIngredient.setRecipe(recipe);
        recipeIngredient.setIngredient(ingredient);
        recipeIngredient.setQuantity(ingredientForm.getIngredientQuantity());
        recipeIngredient.setUnit(ingredientForm.getIngredientUnit());
        recipeIngredient.setCode(Util.generateCode(PrefixCode.RECIPE_INGREDIENT));
        recipeIngredientRepository.save(recipeIngredient);
    }

    private Ingredient getOrCreateIngredient(IngredientForm ingredientForm) {
        Ingredient ingredient = ingredientService.getIngredientFromName(ingredientForm.getIngredientName());
        if(ingredient == null){
            ingredient = new Ingredient();
            //ingredient = ingredientService.save(ingredientForm.getIngredientName());
            ingredient.setName(ingredientForm.getIngredientName());
            ingredient.setCode(Util.generateCode(PrefixCode.INGREDIENT));
            ingredientRepository.save(ingredient);
        }
        return ingredient;
    }

    @Override
    public RecipeDto getByCode(String code) {
        return super.getByCode(code);
    }


    @Override
    public RecipeDto detailsRecipe(String code) {
        RecipeDto recipeDto = this.getByCode(code);
        Recipe recipe = new Recipe();
        // TODO Revoir ce code? Que fait-il exactement?
        /*
        if (recipeDto != null) {
            recipe = this.getMapper().fromDtoToEntity(recipeDto);
        }
        recipeDto = this.getMapper().fromEntityToDto(recipe);

        */
        return recipeDto;

    }



    public Page<RecipeDto> findAllRecipe(int page, int length) {
        Pageable pageRecipe = PageRequest.of(page, length);


        Page<Recipe> recipePage = recipeRepository.findAll(pageRecipe);
        return fromEntityPageToDtoPageable(recipePage);
    }

    private Page<RecipeDto> fromEntityPageToDtoPageable (Page<Recipe> recipePage) {
        List<RecipeDto> recipeDtos = recipePage.stream().map(getMapper()::fromEntityToDto).collect(Collectors.toList());
        Page<RecipeDto> dtoPage = new PageImpl<>(recipeDtos, recipePage.getPageable(), recipePage.getTotalElements());
        return dtoPage;
    }
}
