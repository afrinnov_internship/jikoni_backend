package com.afrinnov.jikoni.recipe;

import com.afrinnov.jikoni.commons.AbstractEntity;
import com.afrinnov.jikoni.ingredient.Ingredient;
import com.afrinnov.jikoni.recipeIngredient.RecipeIngredient;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "recipes")
public class Recipe extends AbstractEntity {

    @Column(name = "label", nullable = false, updatable = false)
    private String label;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "image_path")
    private String imagePath;

    @OneToMany(mappedBy = "recipe")
    private List<RecipeIngredient> recipeIngredients;

}

